class Statistik:
  """
  Class represents a statistical category of the SEM.
  
  The SEM publishes asylum statistics where several variables are
  grouped and published as Excel Files. For example 7-30: Asylprozess 
  und Rückkehrunterstützung Asyl: Ein- und Austritte. 
  Note that categories can change over time. 

  """
  def __init__(self, file_name_with_extension:str, from_year: int, until_year: int, yearly_or_monthly: str, sem_code: str, flow_or_stock: str):
    """
    Parameters:
      file_name_with_extension(str): string of the excel file 
        name including extension.
      from_year(int): year when excel is first published
      unti_year(int): year when excel file is last published. 
        TODO: make it possible to leave empty to be able to define current year at a different place.
      year_or_monthly(str): publishing cycle of Excel File. 
        either 'yearly' or 'monthly'
      sem_code(str): First four to seven characters of SEM code. 
        For example 7-30, 7-80-RU or 7-80-VU. This code is used 
        to combine statistics by their column number.
      flow_or_stock(str): Bestand or Bewegung. Either 'flow' or 'stock'
    """
    
    self.file_name = file_name_with_extension
    self.from_year = from_year
    self.until_year = until_year
    self.yearly_or_monthly = yearly_or_monthly
    self.sem_code = sem_code
    self.flow_or_stock = flow_or_stock