import classes

list_of_excels_from_sem_website = (
    classes.Statistik('7-30-Bew-Aufenthalte-J-d-1994-12.xlsx', 1994, 2018, 'yearly', '7-30', 'flow'), 
    classes.Statistik('7-30-Bew-Aufenthalte-Asyl-J-d-2019-12.xlsx', 2019, 2020, 'yearly', '7-30', 'flow'),
    classes.Statistik('7-31-Bew-Aufenthalte-AIG-J-d-2020-12.xlsx', 2019, 2020, 'yearly', '7-31', 'flow'),
    classes.Statistik('7-50-Bew-Dublin-J-d-2019-12.xlsx', 2009, 2020, 'yearly', '7-50', 'flow'),
    classes.Statistik('7-55-Bew-RueA-J-d-2020-12.xlsx', 2014, 2020, 'yearly', '7-55', 'flow'),
    classes.Statistik('7-80-Bew-VU-J-d-2010-12.xlsx', 2000, 2018, 'yearly', '7-80-VU', 'flow'),
    classes.Statistik('7-80-Bew-RU-Total-J-d-2020-12.xlsx', 2019, 2020, 'yearly', '7-80-RU', 'flow'),
    classes.Statistik('7-81-Bew-RU-Asyl-J-d-2019-12.xlsx', 2019, 2020, 'yearly', '7-81', 'flow'),
    classes.Statistik('7-82-Bew-RU-AIG-J-d-2019-12.xlsx', 2019, 2020, 'yearly', '7-82', 'flow'),
    classes.Statistik('6-10-Best-Asylprozess-d-1994-12.xlsx', 1994, 2020, 'yearly', '6-10', 'stock'),
    )

## single variables deprecated, to be replaced by tuple:
statistik_7_30_until_2018 = classes.Statistik('7-30-Bew-Aufenthalte-J-d-1994-12.xlsx', 1994, 2018, 'yearly', '7-30', 'flow')
statistik_7_30_from_2019 = classes.Statistik('7-30-Bew-Aufenthalte-Asyl-J-d-2019-12.xlsx', 2019, 2020, 'yearly', '7-30', 'flow')
statistik_7_31 = classes.Statistik('7-31-Bew-Aufenthalte-AIG-J-d-2020-12.xlsx', 2019, 2020, 'yearly', '7-31', 'flow')
statistik_7_50 = classes.Statistik('7-50-Bew-Dublin-J-d-2019-12.xlsx', 2009, 2020, 'yearly', '7-50', 'flow')
statistik_7_55 = classes.Statistik('7-55-Bew-RueA-J-d-2020-12.xlsx', 2014, 2020, 'yearly', '7-55', 'flow')
statistik_7_80_until_2018 = classes.Statistik('7-80-Bew-VU-J-d-2010-12.xlsx', 2000, 2018, 'yearly', '7-80', 'flow')
statistik_7_80_from_2019 = classes.Statistik('7-80-Bew-RU-Total-J-d-2020-12.xlsx', 2019, 2020, 'yearly', '7-80', 'flow')
statistik_7_81 = classes.Statistik('7-81-Bew-RU-Asyl-J-d-2019-12.xlsx', 2019, 2020, 'yearly', '7-81', 'flow')
statistik_7_82 = classes.Statistik('7-82-Bew-RU-AIG-J-d-2019-12.xlsx', 2019, 2020, 'yearly', '7-82', 'flow')
statistik_6_10 = classes.Statistik('6-10-Best-Asylprozess-d-1994-12.xlsx', 1994, 2020, 'yearly', '6-10', 'stock')

header_nation:str = 'Nation'
header_datum: str = 'Datum'
header_file_name: str = 'file_name'
header_name_of_sem_statistics: str = 'name_of_sem_statistics'
header_sem_code:str = 'name_of_sem_code'
header_flow_or_stock: str = 'flow_or_stock'
header_yearly_or_monthly: str = 'yearly_or_monthly'

columns_to_keep: list =  [header_nation, header_datum]
header_variable: str = 'Variable' 
urlSem: str = 'https://www.sem.admin.ch/dam/sem/de/data/publiservice/statistik/asylstatistik/'
sheet_name = 'CH-Nati'
csv_file_name_and_column_to_extract = {'7-30-combined': ['RückführungenDublinstaat', 'Rückführungen Drittstaat', 'Rückführungen Heimatstaat', 'Unkontrollierte Abreisen', 'Kontrollierte, selbständige Ausreisen'], '7-80-combined': ['BeendigungenAusreiseorganisation', 'BeendigungenAusreiseorganisation.1', 'AbschlussAusreiseorganisation'], '7-50-Bew-Dublin-J': ['ÜberstellungenOutVerfahren' ], '7-55-Bew-RueA-J':['ÜberstellungenOutVerfahren'], '7-31-Bew-Aufenthalte-AIG-J': ['RückführungenDublinstaat', 'Rückführungen Drittstaat', 'Unkontrollierte Abreisen', 'Rückführungen Heimatstaat', 'Kontrollierte, selbständige Ausreisen'], '7-82-Bew-RU-AIG-J': ['AbschlussAusreiseorganisation'], '7-82-Bew-RU-AIG-J': ['AbschlussAusreiseorganisation'], '6-10-Best-Asylprozess':['Total', 'Total.1']}