from datetime import datetime #use time and date
import requests #used to download excel files
import time #used to wait one second after each excel download
import glob #used to list all files within directory
import pandas as pd
import re #regular expressions
import variables #contains variables
from typing import List, Any #python types
import classes
import string

################
################
# Top Functions, called in Notebooks
################
################

def test():
    """
    Function to test testing. Can be removed once tests are setup.
    """

    return 1

def return_list_of_existing_excels():
    """
    Go through excel_files directory and return list of all excels.
    
    Returns:
        excelsFormatted(list): List which contains strings of excel file names
    """

    excels = glob.glob(r'../../excel_files/*.xlsx')
    #Remove duplicates - convert into dictionary and back into list because dictionary is no allowed to have duplicates.
    excels = list( dict.fromkeys(excels) )
    excelsFormatted: list = list(map(lambda name_of_excel: name_of_excel[18:], excels)) 
    return excelsFormatted
    
def download_excel_files_yearly(statistik):
    """
    Download Excels files from SEM Website. If files exists it skips.
    
    Parameters:
        statistik(classes.Statistik): defined in variables.py. TODO: type annotation correct in this line?
        For a more Elaborate Description of statistik see classes.Statistik
    """

    file_name_without_year = statistik.file_name[:-12]
    existing_excels = return_list_of_existing_excels()
    download_excels_if_not_already_downloaded(statistik, file_name_without_year, existing_excels)
    print('done')

def extract_one_column_from_dataframe_and_save_csv(a_dict):
    """
    Extract variables from different CSV's and save as CSV.

    Import a CSV and extract one columns and append it to a dataframe. Repeat for the entire dictionary.

    Parameters:
        a_dict(dict): Key contains name of Statistic as string. Value contains Column to extract. TODO: type annotiation in this line correct?
    """

    pathToCsv = '../../csv_files/modified_csv_files/plotting_several_yearly_statistics.csv'
    for key, value in a_dict.items():
        df = pd.read_csv('../../csv_files/raw_csv_files/' + key + '.csv')
        for i in value:
            df_new = df.loc[:,variables.columns_to_keep + [i]]
            df_new.rename(columns={i:'Values'}, inplace = True)
            df_new[variables.header_variable] = key[:5] + i 
            if key == list(a_dict.keys())[0] and i == value[0]:
                df_combined = df_new
            else:
                df_combined = df_combined.append(df_new)
    df_combined = df_combined.sort_values('Datum') 
    df_combined.reset_index(inplace=True, drop=True) 
    df_combined.to_csv(pathToCsv) 
    print('done')

################
################
# Sub Functions which are called by Top Functions
# And Smaller Functions
################
################

def download_excels_if_not_already_downloaded(statistik, file_name_without_year, existing_excels): 
    """
    Download excels files which have not been downloaded already.

    Parameters:
        statistik(classes.Statistik): defined in variables.py. TODO: type annotation correct in this line?
        file_name_without_year(str): Excel file name without year. For example '7-30-Bew-Aufenthalte-' without 'J-d-1994-12.xlsx'
        existing_excels(list): list of excel files which exist in /excel_files directory.
    """

    for i in range(statistik.from_year, statistik.until_year + 1 ):
        url = variables.urlSem + str(i) + '/12/' + file_name_without_year + str(i) + '-12.xlsx.download.xlsx/' + file_name_without_year + str(i) + '-12.xlsx'
        if not any(item in url for item in existing_excels):
            r = requests.get(url)
            if r.status_code == 200:
                open('../../excel_files/' + url[-len(statistik.file_name):], 'wb').write(r.content) 
                print(url[-len(statistik.file_name):] + ' was downloaded')
                time.sleep(1)
            else:
                print('error downloading ' + url[-len(statistik.file_name):])

def import_all_excel_files_into_one_dataframe(excels, statistik):
    """
    note: deprecated
    Read a list of excel files into a dataframe and add column with date of excel file and return dataframe

    Parameters:
        
    """

    for k in excels:
        date_from_file_name = pd.to_datetime(k[:-5][-7:]) 
        df = pd.read_excel(k, sheet_name=variables.sheet_name, skiprows=15, header=None)

        df[variables.header_datum] = date_from_file_name 
        df[variables.header_file_name] = statistik.file_name 
        #df[variables.header_name_of_sem_statistics] = name_of_sem_statistics(statistik)
        df[variables.header_sem_code] = statistik.sem_code
        df[variables.header_flow_or_stock] = statistik.flow_or_stock
        df[variables.header_yearly_or_monthly] = statistik.yearly_or_monthly

        if k == excels[0]: 
            df_combined = df # In der ersten Iteration wird das erste Excel direkt in df_combined geschrieben.
        else:
            df_combined = df_combined.append(df) # Ab der zweiten Iteration wird das dataframe an df_combined angehängt. 
    return df_combined

def create_headers(excels, statistics):
    """
    note: deprecated
    Create dataframe of first excel file of excels list and read header of this excel file. return header.
    """
    df_header = pd.read_excel(excels[0], sheet_name=variables.sheet_name)
    header_from_df = df_header.values[2].tolist()
    #df_width =  df
    variable_code_prefix = f'{statistics.sem_code}_{statistics.yearly_or_monthyl}_{statistics.flow_or_stock}_'
    header = [x for x in header_from_df]
    #header = [(re.sub(r'(\n|-|nan)', '', str(x))) for x in header_from_df]
    print(header_from_df)
    header[0] = variables.header_nation
    print(header)
    return header

def create_list_of_all_existing_yearly_excel_files(statistik_name):
    """
    Read directory and create list which contains all existing excel files of certain statistik
    """
    excels = glob.glob('../../excel_files/' + statistik_name + '*.xlsx')
    excels = list( dict.fromkeys(excels) ) #Remove duplicates - convert into dictionary and back into list because dictionary is no allowed to have duplicates.
    return excels

def remove_total_of_continents(df):
    """
    note:deprecated
    The excel files contain total of continents. Remove."""
    df = df[(df.Nation.str.match(r'Total.*')==False) ]
    return df

def drop_row_if_nation_is_nan(df_combined):
    """Drop NaN Values of Dataframe"""
    df_combined.dropna(subset = ['Nation'], inplace=True)

def reset_index(df):
    """Reset index of dataframe"""
    df.reset_index(drop=True, inplace=True)

    
    