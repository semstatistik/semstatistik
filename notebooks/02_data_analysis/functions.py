import plotly.express as px
import seaborn as sns
from matplotlib import pyplot as plt
import matplotlib.ticker as ticker

def plot_one_variable(df, filter_variable: str, filter_nations: list):
    # filter dataframe
    df_filtered = df[df['Nation'].isin(filter_nations)].loc[(df['variable'] == filter_variable)]

    # plot
    plt.figure(figsize=(15, 8))
    ax = sns.lineplot(data=df_filtered, x="Datum", y="value", hue='Nation', palette="Set1")
    #ax.legend(loc='upper right', bbox_to_anchor=(2, 1))
    ax.set(title=filter_variable)
    ax.xaxis.set_major_locator(ticker.LinearLocator(15))


def plot_overview(df, title: str):
    fig = px.line(df, x="Datum", y="Values", line_group="Nation", color="Nation", facet_col="Variable", facet_col_wrap=1, facet_row_spacing=0.01, height=2500, title=title)
    #fig.update_yaxes(matches=None) # Make Zoom of axis independent from each other
    fig.update_layout(
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label="1m",
                        step="month",
                        stepmode="backward"),
                    dict(count=6,
                        label="6m",
                        step="month",
                        stepmode="backward"),
                    dict(count=1,
                        label="YTD",
                        step="year",
                        stepmode="todate"),
                    dict(count=1,
                        label="1y",
                        step="year",
                        stepmode="backward"),
                    dict(step="all")
                ])
            ),
            rangeslider=dict(
                visible=True
            ),
            type="date"
        )
    )
    fig.for_each_annotation(lambda a: a.update(text=a.text.replace('Variable=', '')))

    fig.show()



def plot_one_variable_plotly(df, filter_variable: str, filter_nation: str):
    df_filtered = df[df.Nation.str.contains(filter_nation) & df.Variable.str.contains(filter_variable) ] #filter values
    fig = px.line(df_filtered, x="Datum", y="Values", line_group="Nation", color="Nation", title=filter_variable)
    fig.update_layout(
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label="1m",
                        step="month",
                        stepmode="backward"),
                    dict(count=6,
                        label="6m",
                        step="month",
                        stepmode="backward"),
                    dict(count=1,
                        label="YTD",
                        step="year",
                        stepmode="todate"),
                    dict(count=1,
                        label="1y",
                        step="year",
                        stepmode="backward"),
                    dict(step="all")
                ])
            ),
            rangeslider=dict(
                visible=True
            ),
            type="date"
        )
    )
    fig.for_each_annotation(lambda a: a.update(text=a.text.replace('Variable=', '')))

    fig.show()