# Summary

The aim of this project is to present a comprehensible presentation of deportation-related figures of the `Staatssekretariat für Migration` (SEM). This is intended to illustrate the systematic nature of deportations and to create a greater understanding of the "black box deportation". Our discussion does not take place on a legal or personal level, but refers to figures from the [SEM asylum statistics](https://www.sem.admin.ch/sem/de/home/publiservice/statistik/asylstatistik/archiv.html). This is meant to complement personal and legal approaches in order to place these approaches in a broader context. A comprehensible presentation of deportation-related information is to be made available to various actors for public relations work.

# Project Overview
**Scraping and Saving**
- `notebooks/import_and_prepare_data/import_and_prepare_data.ipynb` will scrape (download) Excel files from [SEM asylum statistics](https://www.sem.admin.ch/sem/de/home/publiservice/statistik/asylstatistik/archiv.html). The downloaded Excel files are stored in `excel_files` and merged as CSVs in the folder `csv_files`.
- The desired Excel files are defined in `notebooks/import_and_prepare_data/variables.py`. Until now, only yearly Excel files (not monthly) can be requested.

**Plotting**
- `notebooks/plotting.ipynb` uses the created CSV files to plot graphs.

# Technical Help
Please refer to the [Wiki](https://gitlab.com/semstatistik/semstatistik/-/wikis/technical-help)

# Technical Notes
## pip3 packages
- openpyxl
- plotly
- pandas
- (-nbformat)
- pytest
- mypy
- [nbstripout](https://github.com/kynan/nbstripout)
    - setup: go to project directory and `nbstripout --install` then `nbstripout --install --attributes .gitattributes`.
    - before using check if installed: `nbstripout --is-installed`
- matplotlib
## git
- before pushing to the repo, output is stripped using [nbstripout](https://github.com/kynan/nbstripout).

## Maybe useful:
- RISE ("Live" Reveal.js Jupyter/IPython Slideshow Extension)
